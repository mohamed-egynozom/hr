<?php

namespace App\Http\Controllers;

use App\Vacation;
use App\User;
use App\Collection;
use App\Http\Requests\StoreVacation;
use App\Http\Requests\UpdateVacation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VacationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->role_id == 1){
            $data['vacations'] = Vacation::all();    
        }else{
            $data['vacations'] = Vacation::where('user_id', Auth::user()->id)->get();
        }
        return view('Vacation.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['route'] = route('vacations.store');
        $data['method'] = 'POST';
        $data['mode'] = 'New';
        return view('Vacation.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\StoreVacation  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreVacation $request)
    {
        $vacation = new Vacation();

        $vacation->start_date = $request->start_date;
        $vacation->end_date = $request->end_date;
        $vacation->days = $request->days;
        $vacation->reason = $request->reason;
        $vacation->user_id = Auth::user()->id;
        $vacation->status = 'PENDING';

        $vacation->save();
        
        $data['flush'] = 'Vacation request created successfully';
        return redirect()->route('vacations.index')->with($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Vacation  $vacation
     * @return \Illuminate\Http\Response
     */
    public function show(Vacation $vacation)
    {
        $data['vacation'] = $vacation;
        return view('Vacation.page', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Vacation  $vacation
     * @return \Illuminate\Http\Response
     */
    public function edit(Vacation $vacation)
    {
        if($vacation->user_id != Auth::user()->id){
            abort(403, 'You are not authorized to access this page, please check system admin.');
        }

        $data['vacation'] = $vacation;
        $data['route'] = route('vacations.update', ['vacations' => $vacation->id]);
        $data['method'] = 'PUT';
        $data['mode'] = 'Update';
        return view('Vacation.form', $data);
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\UpdateVacation  $request
     * @param  \App\Vacation  $vacation
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateVacation $request, Vacation $vacation)
    {
        if($vacation->user_id != Auth::user()->id){
            abort(403, 'You are not authorized to access this page, please check system admin.');
        }

        $vacation->start_date = $request->start_date;
        $vacation->end_date = $request->end_date;
        $vacation->days = $request->days;
        $vacation->reason = $request->reason;

        $vacation->save();
        
        $data['flush'] = 'Vacation updated successfully';
        return redirect()->route('vacations.index')->with($data);
    }

    /**
     * Approve specified vacation.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function approve($id)
    {
        $vacation = Vacation::find($id);
        
        $vacation->status = 'APPROVED';

        $vacation->save();

        $user = User::find($vacation->user_id);
        $user->vacations_balance--;

        $user->save();
        
        $data['flush'] = 'Vacation approved successfully';
        return redirect()->route('vacations.index')->with($data);
    }

    /**
     * Reject specified vacation.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function reject($id)
    {
        $vacation = Vacation::find($id);
        
        if($vacation->status == 'APPROVED'){
            $user = User::find($vacation->user_id);
            $user->vacations_balance++;
            $user->save();
        }

        $vacation->status = 'REJECTED';

        $vacation->save();


        
        $data['flush'] = 'Vacation rejected successfully';
        return redirect()->route('vacations.index')->with($data);
    }

   

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Vacation  $vacation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vacation $vacation)
    {
        if($vacation->user_id != Auth::user()->id){
            abort(403, 'You are not authorized to access this page, please check system admin.');
        }
        $vacation->destroy($vacation->id);
        $url = route('vacations.index');
        return $url;
    }
}
