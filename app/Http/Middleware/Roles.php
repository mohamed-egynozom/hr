<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Roles
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $roles = array_slice(func_get_args(), 2);
        if(Auth::User() && in_array(Auth::User()->role_id, $roles))
        {
            switch(Auth::User()->status_id)
            {
                case '2':
                    abort(403, 'Your Account Pending, please wait system admin approval. !!');
                    break;

                case '3':
                    abort(403, 'Your Account Suspended, Please check system admin. !!');
                    break;

                default:
                    #code
                    break;
            }

            return $next($request);
        }
        else
        {
            abort(403, 'You are not authorized to access this page, please check system admin.');
        }
    }
}
