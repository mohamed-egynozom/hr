<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Request;

class UpdateVacation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation message that return from rules
     * 
     * @return array
     */
    public function messages()
    {
        $messages = [];
        $dataFields = Request::all();

        $messages['start_date.required'] = 'field is required';
        $messages['start_date.date'] = 'must be date';
        $messages['start_date.date_format'] = 'must be date format yyyy-mm-dd';
        $messages['start_date.after_or_equal'] = 'must be current date or future';
        $messages['end_date.required'] = 'field is required';
        $messages['end_date.date'] = 'must be date';
        $messages['end_date.date_format'] = 'must be date format yyyy-mm-dd';
        $messages['end_date.after'] = 'must be after start date';
        $messages['days.required'] = 'field is required';
        $messages['days.numeric'] = 'must be numeric';
        $messages['days.gt'] = 'must be greater than 0';
        $messages['reason.required'] = 'field is required';

        return $messages;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];

        $dataFields = Request::all();
        
        $rules['start_date'] = 'required|date|date_format:Y-m-d|after_or_equal:'.date('Y-m-d');
        $rules['end_date'] = 'required|date|date_format:Y-m-d|after:'.Input::get('start_date');
        $rules['days'] = 'required|numeric|gt:0|lte:'.Auth::user()->vacations_balance;
        $rules['reason'] = 'required';

        return $rules;
    }
}
