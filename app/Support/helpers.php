<?php

/**
 * function to check user role
 * @param $roleId
 * @return boolean true or false
 */
if(!function_exists(('haveRole')))
{
    function haveRole($roleId)
    {
        return Auth::User()->role_id == $roleId ? true : false;
    }
}





