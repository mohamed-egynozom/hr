<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class PayrollSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payrolls')->delete();

        $jan = array(
            'user_id' => '2',          
            'total' => '1000',          
            'currency' => 'EGP',          
            'created_at' => date("Y-m-d H:i:s", strtotime('2021-02-01')),
            'updated_at' => date("Y-m-d H:i:s", strtotime('2021-02-01')),                  
        );

        DB::table('payrolls')->insert($jan);

        $feb = array(
            'user_id' => '2',          
            'total' => '1000',          
            'currency' => 'EGP',          
            'created_at' => date("Y-m-d H:i:s", strtotime('2021-03-01')),
            'updated_at' => date("Y-m-d H:i:s", strtotime('2021-03-01')),                  
        );

        DB::table('payrolls')->insert($feb);

        $feb2 = array(
            'user_id' => '3',          
            'total' => '3000',          
            'currency' => 'EGP',          
            'created_at' => date("Y-m-d H:i:s", strtotime('2021-03-01')),
            'updated_at' => date("Y-m-d H:i:s", strtotime('2021-03-01')),                  
        );

        DB::table('payrolls')->insert($feb2);

        $mar = array(
            'user_id' => '2',          
            'total' => '1000',          
            'currency' => 'EGP',          
            'created_at' => date("Y-m-d H:i:s", strtotime('2021-04-01')),
            'updated_at' => date("Y-m-d H:i:s", strtotime('2021-04-01')),                
        );

        DB::table('payrolls')->insert($mar);
    }
}
