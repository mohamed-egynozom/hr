<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        $managerUser = array(
            'name' => 'manager',          
            'email' => 'manager@hr.com',          
            'password' => Hash::make('Manager@1234'),          
            'first_name' => 'Direct',          
            'last_name' => 'Manager',          
            'role_id' => 1,   
            'vacations_balance' => 21, 
        );

        DB::table('users')->insert($managerUser);

        $employeeUser = array(
            'name' => 'employee',          
            'email' => 'employee@hr.com',          
            'password' => Hash::make('Employee@1234'),          
            'first_name' => 'Employee',          
            'last_name' => 'User',          
            'role_id' => 2,        
            'vacations_balance' => 21,
        );

        DB::table('users')->insert($employeeUser);

        $employeeUser2 = array(
            'name' => 'employee2',          
            'email' => 'employee2@hr.com',          
            'password' => Hash::make('Employee@1234'),          
            'first_name' => 'Employee',          
            'last_name' => 'User',          
            'role_id' => 2,        
            'vacations_balance' => 21,
        );

        DB::table('users')->insert($employeeUser);
    }
}
