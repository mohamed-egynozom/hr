@extends('layouts.egy')

@section('title')
Invoices Managment
@endsection

@section('style')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{ url('/public') }}/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="{{ url('/public') }}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
{{-- custom style --}}
<style>
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
        vertical-align : middle;
        text-align : center;
    }
</style>
@endsection

@section('script')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{ url('/public') }}/assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="{{ url('/public') }}/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="{{ url('/public') }}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ url('/public') }}/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
{{-- Custom script --}}
<script>
    var baseurl = '{{url('/')}}';
    $(document).ready(function(){
        $(".updateItem").click(function(e) {
            var id = $(this).data('id');
            var column = $(this).data('column');
            var value = $(this).data('value');
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            text: "You won't be able to change "+column+" of invoice to be "+value,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, change it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: baseurl+'/invoices/'+id,
                        type: 'PUT',
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "id": id,
                            "column": column,
                            "value": value,
                        },
                        success: function(result) {
                            swalWithBootstrapButtons.fire(
                                'Updated!',
                                'Invoice has been Updated.',
                                'success'
                            )
                            window.location.replace(result);

                        }
                    });
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                    'Cancelled',
                    'Your invoice not changed :)',
                    'error'
                    )
                }
            })
        });
    });
</script>
@endsection

@section('content')
@if(session('flush'))
<div class="alert alert-success"> {{session('flush')}} </div>
@endif
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase"> Payrolls List</span>
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                    <thead>
                        <tr>
                            <th> User </th>
                            <th> Total </th>
                            <th style="padding: 0 70px;"> Created at </th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($payrolls))
                        @foreach ($payrolls as $payroll)
                        <tr class="odd gradeX">
                            <td> {{$payroll->user->first_name}} {{$payroll->user->last_name}}</td>
                            <td> {{$payroll->total}}</td>
                            
                            <td> {{date('Y-m-d',strtotime($payroll->created_at))}}</td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
@endsection
