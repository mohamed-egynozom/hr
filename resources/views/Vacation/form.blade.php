@extends('layouts.egy')

@section('title')
Vacations Managment
@endsection

@section('style')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{ url('/public') }}/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
@endsection

@section('script')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{ url('/public') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
{{-- Custom Script --}}
<script type="text/javascript">
    $('.date-picker').datepicker({
        atutoclose : true,
        format : 'yyyy-mm-dd',
    });

    $(document).ready(function(){
        var start_date;
        var end_date;
        var days;

        $('#start_date').change(function(){
            start_date = new Date($('#start_date').val());
            end_date = new Date($('#end_date').val());
            days = (end_date.getTime() - start_date.getTime())/ (1000 * 3600 * 24);
            $('#days').val(days)
        });

        $('#end_date').change(function(){
            start_date = new Date($('#start_date').val());
            end_date = new Date($('#end_date').val());
            days = (end_date.getTime() - start_date.getTime())/ (1000 * 3600 * 24);
            $('#days').val(days)
        });
    });
</script>
@endsection

@section('content')
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green">
                    <i class="icon-pin font-green"></i>
                    <span class="caption-subject bold uppercase"> {{$mode}} Vacation</span>
                </div>
            </div>
            <div class="portlet-body form">
            <form role="form" action="{{$route}}" method="POST">
                @method($method)
                @csrf
                <div class="form-body">
                    <div class="form-group form-md-line-input form-md-floating-label @error('start_date') has-error @enderror">
                        <input class="form-control date-picker" size="16" type="text" name="start_date" id="start_date" value="{{old('start_date', isset($vacation->start_date) ? date('Y-m-d', strtotime($vacation->start_date)) : null)}}" />
                        <label for="form_control_1">Start date @error('start_date') {{$message}} @enderror</label>
                        <span class="help-block"> Select start date of vacation </span>
                    </div>

                    <div class="form-group form-md-line-input form-md-floating-label @error('end_date') has-error @enderror">
                        <input class="form-control date-picker" size="16" type="text" name="end_date" id="end_date" value="{{old('end_date', isset($vacation->end_date) ? date('Y-m-d', strtotime($vacation->end_date)) : null)}}" />
                        <label for="form_control_1">End date @error('end_date') {{$message}} @enderror</label>
                        <span class="help-block"> Select end date of vacation </span>
                    </div>

                    <div class="form-group form-md-line-input form-md-floating-label @error('days') has-error @enderror">
                        <input type="number" name="days" id="days" value="{{old('days', isset($vacation->days) ? $vacation->days : null)}}" class="form-control" readonly>
                        <label for="form_control_1">@error('days') {{$message}} @enderror</label>
                        <span class="help">This is no of days requested for vacation</span>
                    </div>

                    <div class="form-group form-md-line-input form-md-floating-label @error('reason') has-error @enderror">
                        <textarea name="reason" class="form-control" rows="10">{{old('reason', isset($vacation->reason) ? $vacation->reason : null)}}</textarea>
                        <label for="form_control_1">Vacation reason @error('reason') {{$message}} @enderror</label>
                    </div>

                    <div class="form-actions noborder">
                        <button type="submit" class="btn blue">Submit</button>
                        <button type="reset" class="btn default">Cancel</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->
    </div>
</div>
@endsection
