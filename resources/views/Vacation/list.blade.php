@extends('layouts.egy')

@section('title')
Vacations Managment
@endsection

@section('style')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{ url('/public') }}/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="{{ url('/public') }}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
{{-- custom style --}}
<style>
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
        vertical-align : middle;
        text-align : center;
    }
    .table>tbody>tr>td img {
        width : 180px;
    }
    .default{
        color : #fff;
    }
    .primary{
        color : #337ab7;
    }
    .success{
        color : #36c6d3;
    }
    .lable-APPROVED{
        background-color: #36c6d3;
    }
    .info{
        color : #659be0;
    }
    .warning{
        color : #F1C40F;
    }
    .lable-PENDING{
        background-color: #F1C40F;
    }
    .danger{
        color : #ed6b75;
    }
    .lable-REJECTED{
        background-color: #ed6b75;
    }
</style>
@endsection

@section('script')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{ url('/public') }}/assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="{{ url('/public') }}/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="{{ url('/public') }}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ url('/public') }}/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
{{-- Custom script --}}
<script>
    var baseurl = '{{url('/')}}';
    $(document).ready(function(){
        $(".removeItem").click(function(e) {
            var id = $(this).data('id');
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            text: "You won't be able to delete this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: baseurl+'/vacations/'+id,
                        type: 'DELETE',  // user.destroy
                        data: {
                            "_token": "{{ csrf_token() }}",
                        },
                        success: function(result) {
                            swalWithBootstrapButtons.fire(
                                'Deleted!',
                                'Vacation request has been deleted.',
                                'success'
                            )
                            window.location.replace(result);

                        }
                    });
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                    'Cancelled',
                    'Your imaginary file is safe :)',
                    'error'
                    )
                }
            })
        });
    });
</script>
@endsection

@section('content')
@if(session('flush'))
<div class="alert alert-success"> {{session('flush')}} </div>
@endif
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase"> 
                        @if(Auth::user()->role_id == 2) 
                            Your Vacations Balance {{Auth::user()->vacations_balance}} Days
                        @elseif(Auth::user()->role_id == 1) 
                            Vacations List 
                        @endif
                    </span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">
                                <a href="{{route('vacations.create')}}">
                                    <button id="sample_editable_1_new" class="btn sbold green"> Add New
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                    <thead>
                        <tr>
                            <th> From </th>
                            <th> TO </th>
                            <th> Days </th>
                            <th> Reason </th>
                            <th> Status </th>
                            <th> Actions </th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($vacations))
                        @foreach ($vacations as $vacation)
                        <tr class="odd gradeX">
                            <td>
                                {{$vacation->start_date}}
                            </td>
                            <td>
                                {{$vacation->end_date}}
                            </td>
                            <td>
                                {{$vacation->days}}
                            </td>
                            <td>
                                {{$vacation->reason}}
                            </td>
                            <td>
                                <span class="label label-sm lable-{{$vacation->status}}"> {{$vacation->status}} </span>
                            </td>
                            <td>
                                <a href="{{route('vacations.show', $vacation)}}"><i class="icon-eye info"></i></a>
                                @if(Auth::user()->role_id == 2 && $vacation->user_id == Auth::user()->id)
                                    |
                                <a href="{{route('vacations.edit', $vacation)}}"><i class="icon-note success"></i></a> 
                                    | 
                                <a href="javascript:;" class="removeItem" data-id="{{$vacation->id}}"><i class="icon-trash danger"></i> </a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
@endsection
