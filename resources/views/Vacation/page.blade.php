@extends('layouts.egy')

@section('title')
Vacations Managment
@endsection

@section('style')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{ url('/public') }}/assets/pages/css/blog.min.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
@endsection

@section('script')

@endsection

@section('content')
<div class="blog-page blog-content-2">
    <div class="row">
        <div class="col-lg-12">
            <div class="blog-single-content bordered blog-container">
                
                <div class="blog-single-desc">
                    <p>Dear Manager </p>
                    <br>
                    <p> kindly Approved for my vacation request from {{$vacation->start_date}} to {{$vacation->end_date}} within {{$vacation->days}} days for {{$vacation->reason}}</p>
                    <br>
                    <p>Thank you</p>
                    <br>
                    <p>{{$vacation->user->firstname}} {{$vacation->user->lastname}}</p>
                </div>

                @if(Auth::user()->role_id == 1)
                @if($vacation->status == 'PENDING' || $vacation->status == 'REJECTED')
                <div class="form-group">
                    <a href="{{route('vacation-approve', $vacation->id)}}" class="btn green uppercase btn-md sbold btn-block">Approve
                        <i class="fa fa-check"></i>
                    </a>
                </div>
                @endif
                @if($vacation->status == 'PENDING' || $vacation->status == 'APPROVED')
                <div class="form-group">
                    <a href="{{route('vacation-reject', $vacation->id)}}" class="btn red uppercase btn-md sbold btn-block">Reject
                        <i class="fa fa-close"></i>
                    </a>
                </div>
                @endif
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
