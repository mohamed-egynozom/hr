<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware(['roles:1,2']);


/**
 * ROUTES FOR VACATIONS
 */
Route::resource('vacations', 'VacationController')->middleware(['roles:1,2']);
Route::get('vacation/{id}/approve', 'VacationController@approve')->name('vacation-approve')->middleware(['roles:1']);
Route::get('vacation/{id}/reject', 'VacationController@reject')->name('vacation-reject')->middleware(['roles:1']);

/**
 * ROUTES FOR PAYROLLES
 */
Route::resource('payrolls', 'PayrollController')->middleware(['roles:1,2']);
